mod module;

use crate::module::module::{
    md5hash_cash, receive, recover_secret, send, Challenge, ChallengeAnswer, ChallengeResult,
    EndOfGame, MD5HashCashOutput, Message, PublicLeaderBoard, RoundSummary, Subscribe,
    SubscribeError, SubscribeResult, Welcome,
};
use std::net::TcpStream;

fn main() {
    let stream = TcpStream::connect("127.0.0.1:7878");
    match stream {
        Ok(mut stream) => {
            let hello = Message::Hello;
            send(&mut stream, hello);

            let array = [0; 4];

            loop {
                let challenge = receive(&mut stream, array);

                match challenge {
                    Ok(v) => {
                        if let Message::Welcome(..) = v {
                            let subscribe = Message::Subscribe(Subscribe {
                                name: "monel_winnie".parse().unwrap(),
                            });
                            send(&mut stream, subscribe);
                        }
                        if let Message::PublicLeaderBoard(..) = v {
                            println!("public")
                        }
                        if let Message::EndOfGame(..) = v {
                            break;
                        }
                        if let Message::Challenge(challenge) = v {
                            println!("challenge a effectué : {:?}", challenge);
                            loop {
                                match challenge.clone() {
                                    Challenge::RecoverSecret(recover) => {
                                        let recover_secret_answer = recover_secret(recover);

                                        let challenge_result =
                                            Message::ChallengeResult(ChallengeResult {
                                                answer: ChallengeAnswer::RecoverSecret(
                                                    recover_secret_answer,
                                                ),
                                                next_target: "monel_winnie".parse().unwrap(),
                                            });
                                        send(&mut stream, challenge_result);
                                    }

                                    Challenge::MD5HashCash(hashcash) => {
                                        let complexity = hashcash.complexity;
                                        let message = hashcash.message;

                                        let md5answer = md5hash_cash(complexity, message);

                                        println!("reponse du challenge {:?}", md5answer);

                                        let challenge_result =
                                            Message::ChallengeResult(ChallengeResult {
                                                answer: ChallengeAnswer::MD5HashCash(md5answer),
                                                next_target: "monel_winnie".parse().unwrap(),
                                            });
                                        send(&mut stream, challenge_result);

                                        break;
                                    }

                                    _ => {}
                                }
                            }
                        }
                    }
                    _ => {
                        println!("{:?}", challenge);
                        break;
                    }
                }
            }
        }
        Err(err) => panic!("Cannot connect: {}", err),
    }
}
